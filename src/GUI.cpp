#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Menu.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Simple_Counter.H>
#include <FL/Fl_Input.H>
#include "Scope.hpp"
#include "GUI.hpp"

#define U 30
#define v 2
#define W 5

#define C(x,y,w,h) U*x+v,U*y+v,U*w-2*v,U*h-2*v

void FileExit(Fl_Widget * widget, Program * program) {
	program->Done();
}

GUI::GUI(int w, int h) {

	Fl_Menu_Item menu[] = { { "File", 0, 0, 0, FL_SUBMENU }, { "Exit", 0,
			(Fl_Callback*) FileExit, this, 0 }, { 0 }, 0 };

	win = new Fl_Double_Window((40 + W) * U, 33 * U, "Atmega328 PC Scope");
	menuBar = new Fl_Menu_Bar(0, 0, (40 + W) * U, 1 * U);
	serialCombo = new Fl_Choice(C(0, 2, W, 1), "Serial port");
	baudInput = new Fl_Input(C(0, 4, W, 1), "Baud rate");
	serialButton = new Fl_Button(C(0, 5, W, 1), "Connect");
	referenceCombo = new Fl_Choice(C(0, 7, W, 1), "Reference");
	channelCounter = new Fl_Simple_Counter(C(0, 9, W, 1), "Channel");
	triggerVoltage = new Fl_Input(C(0, 11, W, 1), "Trigger voltage");
	triggerMode = new Fl_Choice(C(0, 13, W, 1), "Trigger mode");
	traceDivider = new Fl_Input(C(0, 15, W, 1), "Trace divider");
	traceGain = new Fl_Input(C(0, 17, W, 1), "Trace gain");
	traceOffset = new Fl_Input(C(0, 19, W, 1), "Trace offset");
	scope = new Scope(C(W, 1, 40, 32));
	program = nullptr;
	win->end();

	menuBar->copy(menu);

	serialCombo->align(FL_ALIGN_TOP);
	serialCombo->add("/dev/arduino");
	serialCombo->add("/dev/ttyUSB0");
	serialCombo->add("random");
	serialCombo->value(0);

	baudInput->align(FL_ALIGN_TOP);
	baudInput->value("1000000");

	referenceCombo->align(FL_ALIGN_TOP);
	referenceCombo->add("1.1 V");
	referenceCombo->add("AVcc");
	referenceCombo->add("ARef");
	referenceCombo->value(1);

	channelCounter->align(FL_ALIGN_TOP);
	channelCounter->bounds(0.0, 15.0);
	channelCounter->step(1.0);

	triggerVoltage->align(FL_ALIGN_TOP);
	triggerVoltage->value("-0.5");
	triggerVoltage->callback(
			[](Fl_Widget * sender, void *data) {std::cout << (char*)data << std::endl;}, (void*)"TriggerVoltage");
	triggerMode->align(FL_ALIGN_TOP);
	triggerMode->add("Up");
	triggerMode->add("Down");
	triggerMode->add("No trigger");
	triggerMode->value(1);

	traceDivider->align(FL_ALIGN_TOP);
	traceDivider->value("1");

	traceGain->align(FL_ALIGN_TOP);
	traceGain->value("1.0");

	traceOffset->align(FL_ALIGN_TOP);
	traceOffset->value("0.0");
	win->resizable(scope);
	win->show();
}

void GUI::SetProgram(Program* program) {
	this->program = program;
}

void GUI::updateScope(std::vector<double> measurements) {
	scope->update(measurements);
	win->redraw();
}

void GUI::Close(void) {
	win->hide();
}

double GUI::GetGain(void) {
	return atof(traceGain->value());
}

double GUI::GetOffset(void) {
	return atof(traceOffset->value());
}

double GUI::GetTrigger(void) {
	return atof(triggerVoltage->value());
}

int GUI::GetDivider(void)
{
	return atoi(traceDivider->value());
}
