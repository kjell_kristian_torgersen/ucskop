/*
 * Scope.hpp
 *
 *  Created on: Sep 17, 2016
 *      Author: kjell
 */

#ifndef SCOPE_HPP_
#define SCOPE_HPP_

#include <FL/Fl_Box.H>
#include <vector>

class Scope: public Fl_Box {
public:
	Scope();
	Scope(int x, int y, int w, int h, const char *l = 0);
	void update(std::vector<double> view);
	void draw();
private:
	std::vector<double> view;
};

#endif /* SCOPE_HPP_ */
