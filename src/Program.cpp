#include "Program.hpp"
#include <vector>
#include <chrono>
#include <thread>
#include "SerialPort.hpp"

Program::Program(GUI * gui) :
		done(false), gui(gui)
{
	sp.SetBaud(B1000000);
	sp.Open("/dev/ttyUSB0");
}

void Program::Run(void)
{
	std::thread th(&Program::GetMeasurements, this);
	while (!done) {
		Fl::wait();
	}
	gui->Close();
}

void Program::Done(void)
{
	exit(0);
}

void Program::GetMeasurements(void)
{

	std::vector<double> noise;
	ByteArray b;
	int n;
	int j = 0;
	unsigned char buf[1024];
	noise.resize(1024);
	double gain = 1.0;
	double offset;
	double triggerVoltage;
	double voltage;
	double prev;
	double vmax;
	int divcounter = 0;
	int divider = 5;
	bool triggered = false;
	while (!done) {
		Fl::lock();
		gain = gui->GetGain();
		offset = gui->GetOffset();
		triggerVoltage = gui->GetTrigger();
		divider = gui->GetDivider();
		Fl::unlock();
		n = sp.Read(buf, 1024);
		for (int i = 0; i < n; i++) {
			if (divcounter == 0) {
				voltage = (double)((unsigned char) buf[i]);
			} else {
				voltage += (double)((unsigned char) buf[i]);
			}
			divcounter++;
			if (divcounter >= divider) {
				voltage /= 255.0;
				voltage /= (double)divider;
				divcounter = 0;
				if (voltage > triggerVoltage && prev < triggerVoltage) {
					triggered = true;
				}
				prev = voltage;
				//triggered = true;
				if (triggered) {
					noise[j & 1023] = -(voltage * gain + offset);
					if(voltage > vmax) vmax = voltage;
					j++;
					if (j == 1024) {
						std::cout << "Vmax = " << 5.0*vmax <<std::endl;
						vmax = 0.0;
						j = 0;
						triggered = false;
						Fl::lock();

						gui->updateScope(noise);
						Fl::awake();
						Fl::unlock();
					}
				}
			}
		}
	}
}

void Program::UpdateGain(double gain)
{

}

void Program::UpdateOffset(double offset)
{

}

void Program::UpdateTriggerVoltage(double triggerVoltage)
{

}
