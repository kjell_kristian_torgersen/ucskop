/*
 * GUI.hpp
 *
 *  Created on: Sep 17, 2016
 *      Author: kjell
 */

#ifndef GUI_HPP_
#define GUI_HPP_
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Menu.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Simple_Counter.H>
#include <FL/Fl_Input.H>
#include "Scope.hpp"
#include "Program.hpp"

class Program;

class GUI {
public:
	GUI(int w, int h);
	void SetProgram(Program * program);
	void updateScope(std::vector<double> measurements);
	void Close(void);
	double GetGain(void);
	double GetOffset(void);
	double GetTrigger(void);
	int GetDivider(void);
private:
	Fl_Double_Window *win; //((40 + W) * U, 33 * U);
	Fl_Menu_Bar *menuBar; //(0, 0, (40 + W) * U, 1 * U);
	Fl_Choice *serialCombo; //(C(0, 2, W, 1), "Serial port");
	Fl_Input *baudInput; //(C(0, 4, W, 1), "Baud rate");
	Fl_Button *serialButton; //(C(0, 5, W, 1), "Connect");
	Fl_Choice *referenceCombo; //(C(0, 7, W, 1), "Reference");
	Fl_Simple_Counter *channelCounter; //(C(0, 9, W, 1), "Channel");
	Fl_Input *triggerVoltage; //(C(0, 11, W, 1), "Trigger voltage");
	Fl_Choice *triggerMode; //(C(0, 13, W, 1), "Trigger mode");
	Fl_Input *traceDivider; //(C(0, 15, W, 1), "Trace divider");
	Fl_Input *traceGain; //(C(0, 17, W, 1), "Trace gain");
	Fl_Input *traceOffset; //(C(0, 19, W, 1), "Trace offset");
	Scope *scope; //(C(W, 1, 40, 32));
	Program * program;
};

#endif /* GUI_HPP_ */
