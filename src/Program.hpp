/*
 * Program.hpp
 *
 *  Created on: Sep 17, 2016
 *      Author: kjell
 */

#ifndef PROGRAM_HPP_
#define PROGRAM_HPP_

#include "GUI.hpp"
#include "Program.hpp"
#include "SerialPort.hpp"

class GUI;

class Program {
public:
	Program(GUI * gui);
	void Run(void);
	void Done(void);
	void GetMeasurements(void);
	void UpdateGain(double gain);
	void UpdateOffset(double offset);
	void UpdateTriggerVoltage(double triggerVoltage);
private:
	bool done;
	GUI * gui;
	SerialPort sp;
};



#endif /* PROGRAM_HPP_ */
