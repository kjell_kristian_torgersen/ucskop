#include "Scope.hpp"
#include <FL/fl_draw.H>

Scope::Scope() : Fl_Box(0, 0, 1, 1, 0) {
}

Scope::Scope(int x, int y, int w, int h, const char* l) :
		Fl_Box(x, y, w, h, l) {
}

void Scope::update(std::vector<double> view)
{
	this->view = view;
}

void Scope::draw() {
	int wd = w();
	int ht = h();
	int xo = x();
	int yo = y();

	fl_color(FL_BLACK);
	fl_rectf(xo, yo, wd, ht);

	fl_push_matrix();
	fl_translate(xo, (yo + (ht / 2)));
	fl_scale(wd, ht / 2);

	fl_color(FL_DARK1);
	for (int i = 0; i < 5; i++) {
		fl_begin_line();
		fl_vertex((double) i / 5.0, -1);
		fl_vertex((double) i / 5.0, 1);
		fl_end_line();
	}
	for (int i = 0; i < 4; i++) {
		fl_begin_line();
		fl_vertex(-1, (double) (i - 2) / 2.0);
		fl_vertex(1, (double) (i - 2) / 2.0);
		fl_end_line();
	}

	fl_color(FL_GREEN);
	fl_begin_line();
	for (int i = 0; i < view.size(); i++) {
		fl_vertex(((float) i / (float) view.size()), view[i]);
	}
	fl_end_line();

	fl_pop_matrix();
} /* end of draw() method */
