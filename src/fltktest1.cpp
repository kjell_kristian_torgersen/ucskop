#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Menu.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Simple_Counter.H>
#include <FL/Fl_Input.H>
//#include <vector>
//#include <thread>
//#include <math.h>

#include "Program.hpp"
#include "Scope.hpp"
#include "GUI.hpp"
//#include <chrono>


int main(int argc, char **argv) {
	GUI gui(44, 33);
	Program p(&gui);
	gui.SetProgram(&p);
	p.Run();
	return 0;
}
