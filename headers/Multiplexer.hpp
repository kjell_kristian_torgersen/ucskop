/*
 * MultiPlexer.hpp
 *
 *  Created on: Jan 28, 2016
 *      Author: kjell
 */

#ifndef MULTIPLEXER_HPP_
#define MULTIPLEXER_HPP_
#include <vector>
#include <cstddef>
#include "DataSource.hpp"

#include "FixedArray.hpp"
#include "global.hpp"

typedef void (*Callback_t)(void *sender, ByteArray data);

class DataReceived
{
public:
	DataSource * _DataSource;
	Callback_t _Callback;
	DataReceived(DataSource * dataSource, Callback_t callback);
};

typedef std::vector<DataReceived> DataReceivedList;

class Multiplexer: public Object
{
private:
	std::vector<uint8_t> _RxData;
	DataReceivedList _DataReceived;
public:
	Multiplexer(void);
	~Multiplexer();
	Multiplexer(unsigned int readBufferSize);
	void AddDatasource(DataSource * datasource, Callback_t callback);
	void Select(void);
	std::string ToString();
	std::string GetType();

};

#endif /* MULTIPLEXER_HPP_ */
