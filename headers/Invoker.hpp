/*
 * Invoker.hpp
 *
 *  Created on: Sep 3, 2016
 *      Author: kjell
 */

#ifndef INVOKER_HPP_
#define INVOKER_HPP_

#include <queue>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <iostream>
#include <functional>

using namespace std;

typedef function<void(void)> Command;

class Invoker
{
public:
	void Invoke(Command command);
	int QueueSize(void);
	Command GetCommand(void);
	void Wait(void);
	void Abort(void);
private:
	condition_variable cv;
	mutex commandMutex;
	mutex waitMutex;
	queue<Command> commands;
};

#endif /* INVOKER_HPP_ */
