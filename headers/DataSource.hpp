/*
 * DataSource.hpp
 *
 *  Created on: Jan 28, 2016
 *      Author: kjell
 */

#ifndef DATASOURCE_HPP_
#define DATASOURCE_HPP_

#include <iostream>
#include <cstddef>
#include "SafeArray.hpp"
#include <vector>
#include "global.hpp"
#include "Object.hpp"

class DataSource: public Object
{
protected:
	int _fd;
	//uint8_t * buffer;
public:
	DataSource();
	DataSource(int fd);
	~DataSource();
	int Close();
	bool IsOpen();
	void Write(ByteArray array);
	void Write(std::string str);
	void Write(void * data, size_t n);
	size_t Read(unsigned char * buf, size_t maxBytes);
	std::string ReadLine(void);
	ByteArray Read(size_t maxBytes);
	char ReadByte();
	int GetFd() const;
	std::string ToString();
	std::string GetType();
};

#endif /* DATASOURCE_HPP_ */
