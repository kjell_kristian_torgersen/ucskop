/*
 * timer.hpp
 *
 *  Created on: Jan 30, 2016
 *      Author: kjell
 */

#ifndef TIMER_HPP_
#define TIMER_HPP_

#include <unistd.h>
#include <sys/timerfd.h>
#include "DataSource.hpp"
#include "global.hpp"

class Timer : public DataSource
{
private:
	struct itimerspec TimerSettings;
public:
	Timer();
	void SetInterval(__time_t seconds, __syscall_slong_t nanoseconds);
	void SetOnce(__time_t seconds, __syscall_slong_t nanoseconds);
	~Timer();
	std::string ToString();
	std::string GetType();
};

#endif /* TIMER_HPP_ */
