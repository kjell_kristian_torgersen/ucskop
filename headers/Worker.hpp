/*
 * Worker.hpp
 *
 *  Created on: Sep 3, 2016
 *      Author: kjell
 */

#ifndef WORKER_HPP_
#define WORKER_HPP_

#include <thread>

#include "Invoker.hpp"


class Worker : public Invoker {
public:
	Worker();
	void Stop();
private:
	thread workerThread;
	volatile bool done;
	void execute();
};



#endif /* WORKER_HPP_ */
