/*
 * global.hpp
 *
 *  Created on: Feb 6, 2016
 *      Author: kjell
 */

#ifndef GLOBAL_HPP_
#define GLOBAL_HPP_

#undef __cplusplus
#define __cplusplus 20140909UL

#include <vector>
#include <cstdint>



#define USE_GARBAGE_COLLECTOR

#ifdef USE_GARBAGE_COLLECTOR
#define GC_NAME_CONFLICT
#include <gc/gc_cpp.h>
#define newgc new(UseGC)
#else
#define newgc new
#endif


#endif /* GLOBAL_HPP_ */
