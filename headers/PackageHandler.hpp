/*
 * PackageHandler.hpp
 *
 *  Created on: Jan 30, 2016
 *      Author: kjell
 */

#ifndef PACKAGEHANDLER_HPP_
#define PACKAGEHANDLER_HPP_

#include <functional>

#include "SafeArray.hpp"
#include <Object.hpp>
#include "global.hpp"

/** This can be set to whatever byte you want to trigger on*/
#define START_BYTE 0xAA

/** This callback is for functions that should be called when a valid package is received. The content of the Block Package is just the package payload and size. No checksum or anything else.*/
typedef std::function<void(ByteArray)> PackageReceivedCallback;

class PackageHandler : public Object {
private:
	int PackageIndex;
	unsigned int PackageChecksum;
	ByteArray Package;
	PackageReceivedCallback InterpretPackage;
public:
	PackageHandler(PackageReceivedCallback callBack);
	static ByteArray CreatePackage(ByteArray & payload);
	void ReceivePackage(ByteArray& package);
	~PackageHandler();
	std::string ToString();
};

#endif /* PACKAGEHANDLER_HPP_ */
