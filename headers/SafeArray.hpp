/*
 * Array
 *
 *  Created on: Sep 10, 2016
 *      Author: kjell
 */

#ifndef ARRAY_
#define ARRAY_

#include "global.hpp"
#include <exception>
#include <cstdint>
#include <iostream>

template<class T>
class SafeArray
{
public:
	SafeArray()
	{
		array = NULL;
		_size = -1;
	}
	SafeArray(int size)
	{
		this->_size = size;
		if (size != 0) {
			this->array = newgc T[size];
		} else {
			this->array = nullptr;
		}
	}
	T& operator[](int idx)
	{
		if (idx < _size) {
			return array[idx];
		} else {
			//throw std::out_of_range("Array out of bounds");
		}
	}

	int size()
	{
		return _size;
	}

	~SafeArray()
	{
#ifndef USE_GARBAGE_COLLECTOR
		if(array == nullptr) {
			_size = 0;
			return;
		}
		if (_size > 0)
			delete[] (array);
#endif
		array = nullptr;
		_size = -1;
	}

private:
	T * array;
	int _size;
};

typedef SafeArray<uint8_t> ByteArray;
#endif /* ARRAY_ */
