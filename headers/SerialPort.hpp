/*
 * serialport.hpp
 *
 *  Created on: Jan 28, 2016
 *      Author: kjell
 */

#ifndef SERIALPORT_HPP_
#define SERIALPORT_HPP_

#include <DataSource.hpp>
#include <fcntl.h>
#include <stddef.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>
#include "Block.hpp"
#include <string>
#include "global.hpp"

class SerialPort: public DataSource
{
private:
	std::string _Device;
	tcflag_t _Baud;
	struct termios _oldtio; /**< The old settings for the serial port (to set it back to before closing it)*/
public:
	SerialPort(std::string device, tcflag_t baud);
	SerialPort(std::string device);
	SerialPort(tcflag_t baud);
	SerialPort();
	~SerialPort();
	int Open();
	int Open(std::string device);
	int Close();
	std::string GetDevice() const;
	void SetDevice(std::string Device);
	tcflag_t GetBaud() const;
	void SetBaud(tcflag_t Baud);
	std::string ToString();

};

#endif /* SERIALPORT_HPP_ */
