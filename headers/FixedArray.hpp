/*
 * FixedArray.hpp
 *
 *  Created on: Jan 30, 2016
 *      Author: kjell
 */

#ifndef FIXEDARRAY_HPP_
#define FIXEDARRAY_HPP_

#include <sstream>
#include <type_traits>
#include <stdexcept>
//#include "global.hpp"

#include "Object.hpp"

template<class T>
class FixedArray: public Object
{
private:
	T * _Data;     // elements
	unsigned int _Count;
public:
	/** \brief Create a new empty array of size size.
	 * \param size Number of elements */
	FixedArray(unsigned int count)
	{
		unsigned int i;
		_Count = count;
		_Data = newgc T[_Count];
		for (i = 0; i < _Count; i++) {
			_Data[i] = 0;
		}
	}

	/** \brief Create a copy of an existing block data of memory of size elements
	 * \param size Number of elements
	 * \param *data Pointer to existing data.
	 * */
	FixedArray(T * data, unsigned int count)
	{
		unsigned int i;
		_Count = count;
		_Data = newgc T[_Count];
		for (i = 0; i < _Count; i++) {
			_Data[i] = data[i];
		}
	}

	T Value(unsigned int i)
	{
		if (i >= _Count)
			throw newgc std::out_of_range("Trying to access data outside of array limits.");
		return _Data[i];
	}

	/** \brief Access an element in array with bounds checking. Reccomended for normal us
	 * \param i which element to access.
	 * \return Reference to specified element. */
	T& operator[](unsigned int i) const
	{
		if (i >= _Count)
			throw newgc std::out_of_range("Trying to access data outside of array limits.");
		return _Data[i];
	}

	/** \brief Get the size of the array
	 * \return Size in bytes */
	size_t ByteSize() const
	{
		return _Count * sizeof(T);
	}

	/** \brief Get the size of the array
	 * \return Size in number of element */
	unsigned int Count() const
	{
		return _Count;
	}

	~FixedArray()
	{
		//T t = 0;

		//Delete(t);
	}

	template<class C = T>
	void Delete(const C & x)
	{
		// Just delete array
		(void) x;

		if (_Data)
			delete[] (_Data);

	}

	template<class P = T*>
	void Delete(P* p)
	{
		// Delete all elements then delete array
		(void) p;

		unsigned int i;
		for (i = 0; i < _Count; i++) {
			if (_Data[i])

				delete (_Data[i]);
		}
		delete[] (_Data);

	}

	T* Data() const
	{
		return _Data;
	}

	std::string ToString()
	{
		std::stringstream ss;
		ss << FixedArray::GetType() << ": Count=" << _Count << ", Elemsize=" << sizeof(T) <<". " << Object::ToString();
		return ss.str();
	}
	std::string GetType()
	{
		return "FixedArray";
	}

};



#endif /* FIXEDARRAY_HPP_ */
