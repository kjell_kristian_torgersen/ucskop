/*
 * Matrix.hpp
 *
 *  Created on: Jan 31, 2016
 *      Author: kjell
 */

#ifndef MATRIX_HPP_
#define MATRIX_HPP_
#include "global.hpp"

#include <sstream>

template<class T>
class Matrix
{
private:
	T * _Matrix;
	int _Rows;
	int _Cols;
public:
	Matrix(int rows, int cols)
	{
		int i;
		_Rows = rows;
		_Cols = cols;
		_Matrix = newgc T[_Rows * _Cols];
		for (i = 0; i < _Rows * _Cols; i++) {
			_Matrix[i] = 0;
		}
	}

	inline T Index(unsigned int& row, unsigned int& col) const
	{
		if ((row >= _Rows) | (col >= _Cols)) {
			throw newgc std::out_of_range("Trying to access data outside of matrix limits.");
		}
		return _Matrix[row * _Cols + col];
	}

	inline T& operator()(unsigned int row, unsigned int col)
	{
		if ((row >= _Rows) | (col >= _Cols)) {
			throw newgc std::out_of_range("Trying to access data outside of matrix limits.");
		}
		return _Matrix[row * _Cols + col];
	}

	~Matrix()
	{
		delete[] (_Matrix);
	}

	Matrix * operator*(const Matrix &right)
	{
		T tmp;
		Matrix * result = newgc Matrix(this->_Rows, right._Cols);
		unsigned int i, j, k;
		for (i = 0; i < this->_Rows; i++) {
			for (j = 0; j < right._Cols; j++) {
				tmp = 0;
				for (k = 0; k < this->_Cols; k++) {
					tmp += this->Index(i, k) * right.Index(k, j);
				}
				(*result)(i, j) = tmp;
			}
		}
		return result;
	}

	std::string ToString()
	{
		unsigned int i, j;
		std::stringstream ss;
		for (i = 0; i < _Cols; i++) {
			for (j = 0; j < _Rows; j++) {
				ss << Index(i,j) << "  ";
			}
			ss << std::endl;
		}
		return ss.str();

	}
};

#endif /* MATRIX_HPP_ */
