#include <cstring>
#include <cstddef>
#include "Block.hpp"
#include "global.hpp"

Block::Block(unsigned char* data)
{
	Size = 0;
	Data = data;
}


Block::Block(unsigned char* data, size_t size)
{
	Size = size;
#ifdef USE_GARBAGE_COLLECTOR
	Data = new unsigned char [Size];
#else
	Data = new unsigned char[Size];
#endif
	memcpy(Data, data, Size);
}

Block::Block(size_t size)
{
	Size = size;
	Data = new unsigned char[Size];

}

Block::~Block()
{

	if(Size != 0)
		delete[](Data);

}



