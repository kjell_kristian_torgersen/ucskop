/*
 * Worker.cpp
 *
 *  Created on: Sep 3, 2016
 *      Author: kjell
 */
#include "Worker.hpp"

Worker::Worker()
{
	done = false;
	workerThread = thread(&Worker::execute, this);
}

void Worker::Stop()
{
	done = true;
	Abort();
	if(workerThread.joinable())workerThread.join();
}

void Worker::execute()
{

	Command cmd;
	for(;;) {
		while(QueueSize() != 0) {
			cmd = GetCommand();
			if(done) return;
			cmd();
		}
		Wait();
		if(done) return;
	}
}




