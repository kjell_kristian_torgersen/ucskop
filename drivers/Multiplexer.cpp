#include <sstream>
#include <list>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include "Multiplexer.hpp"


#include "FixedArray.hpp"
#include "PackageHandler.hpp"
#include "global.hpp"

Multiplexer::Multiplexer(void)
{
	_RxData.resize(8192);
}

Multiplexer::~Multiplexer()
{

}

Multiplexer::Multiplexer(unsigned int readBufferSize)
{
	_RxData.resize(readBufferSize);
}

void Multiplexer::AddDatasource(DataSource* datasource, Callback_t callback)
{
	_DataReceived.push_back(DataReceived(datasource, callback));
}

void Multiplexer::Select(void)
{
	DataReceivedList::iterator it;
	ssize_t n;
	int maxfd = -1;
	fd_set readfds;
	int fd;
	FD_ZERO(&readfds);

	for (it = _DataReceived.begin(); it != _DataReceived.end(); it++) {
		fd = (*it)._DataSource->GetFd();
		if (fd > maxfd) {
			maxfd = fd;
		}
		FD_SET(fd, &readfds);
	}

	select(maxfd + 1, &readfds, NULL, NULL, NULL);

	for (it = _DataReceived.begin(); it != _DataReceived.end(); it++) {
		fd = (*it)._DataSource->GetFd();
		if (FD_ISSET(fd, &readfds)) {
			/* Check if data available */
			/* Yes, read n bytes and store into ReadBuffer object*/

			n = read(fd, _RxData.data(), _RxData.size());
			ByteArray array((int)n);
			for(int i = 0; i < n; i++) array[i] = _RxData[i];
			(*it)._Callback(it->_DataSource, array); /* Execute user specified callback function*/

			//delete (receivedData);

		}
	}
}

DataReceived::DataReceived(DataSource* dataSource, Callback_t callback)
{
	_DataSource = dataSource;
	_Callback = callback;
}

std::string Multiplexer::ToString()
{
	std::stringstream ss;
	ss << Multiplexer::GetType() << Object::ToString();
	return ss.str();
}

std::string Multiplexer::GetType()
{
	return "Multiplexer";
}
