#include <cstring>
#include <sstream>
#include <stdexcept>

#include "Timer.hpp"
#include "DataSource.hpp"
#include "global.hpp"

Timer::Timer()
{
	bzero(&TimerSettings, sizeof(struct itimerspec));

	_fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	if(_fd < 0) {
		std::__throw_invalid_argument("Cannot create timer");;//("Trying to access data outside of array limits.");
	}

}

void Timer::SetInterval(__time_t seconds, __syscall_slong_t nanoseconds)
{
	TimerSettings.it_interval.tv_sec = seconds;
	TimerSettings.it_interval.tv_nsec = nanoseconds;
	TimerSettings.it_value.tv_sec = seconds;
	TimerSettings.it_value.tv_nsec = nanoseconds;
	timerfd_settime(_fd, 0, &TimerSettings, NULL);
}

void Timer::SetOnce(__time_t seconds, __syscall_slong_t nanoseconds)
{
	TimerSettings.it_interval.tv_sec = 0;
	TimerSettings.it_interval.tv_nsec = 0;
	TimerSettings.it_value.tv_sec = seconds;
	TimerSettings.it_value.tv_nsec = nanoseconds;
	timerfd_settime(_fd, 0, &TimerSettings, NULL);
}

Timer::~Timer()
{
	DataSource::Close();
}

std::string Timer::ToString()
{
	std::stringstream ss;
	double interval = (double)TimerSettings.it_interval.tv_sec+(double)TimerSettings.it_interval.tv_nsec/1e9;
	double once = (double)TimerSettings.it_value.tv_sec+(double)TimerSettings.it_value.tv_nsec/1e9;
	ss << Timer::GetType() << ": Interval = " << interval << "s., Once = " << once << "s. " << DataSource::ToString();

	return ss.str();
}

std::string Timer::GetType()
{
	return "Timer";
}
/*
 * Timer.cpp
 *
 *  Created on: Jan 30, 2016
 *      Author: kjell
 */


