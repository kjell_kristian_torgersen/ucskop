/*
 * Misc.cpp
 *
 *  Created on: Feb 6, 2016
 *      Author: kjell
 */

#include <sstream>
#include <fstream>
#include <vector>
#include "global.hpp"


namespace misc
{
using namespace std;

void WriteAllBytes(string path, const char * bytes, size_t n)
{
	ofstream outfile(path, ios::out | ios::binary);
	outfile.write(bytes, n);
	outfile.close();
}

std::vector<char> ReadAllBytes(string path)
{
	ifstream ifs(path, ios::binary | ios::ate);
	ifstream::pos_type pos = ifs.tellg();

	std::vector<char> result(pos);

	ifs.seekg(0, ios::beg);
	ifs.read(&result[0], pos);

	return result;
}

// You could also take an existing vector as a parameter.
vector<string> split(string str, char delimiter)
{
	vector<string> internal;
	stringstream ss(str); // Turn the string into a stream.
	string tok;

	while (getline(ss, tok, delimiter)) {
		internal.push_back(tok);
	}

	return internal;
}

}
