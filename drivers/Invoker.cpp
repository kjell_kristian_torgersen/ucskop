/*
 * Invoker.cpp
 *
 *  Created on: Sep 3, 2016
 *      Author: kjell
 */

#include "Invoker.hpp"

/** \brief Put a command in the queue for execution.
 * \param command Command to execute later. */
void Invoker::Invoke(Command command)
{
	commandMutex.lock();
	commands.push(command);
	commandMutex.unlock();
	cv.notify_all();
}


/** \brief Get the size of the queue in number of commands. */
int Invoker::QueueSize(void)
{
	int size;
	commandMutex.lock();
	size = commands.size();
	commandMutex.unlock();
	return size;
}

/** \brief Get a command from the queue. Ensure that QueueSize returns something greater then 0 before calling this function. */

Command Invoker::GetCommand(void)
{
	commandMutex.lock();
	Command command = commands.front();
	commands.pop();
	commandMutex.unlock();
	return command;
}

/** \brief */
void Invoker::Wait(void)
{
	unique_lock<mutex> lock(waitMutex);
	cv.wait(lock);
}

/** \brief Abort */
void Invoker::Abort(void)
{
	cv.notify_all();
}
