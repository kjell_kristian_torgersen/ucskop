/*
 * Thread.cpp
 *
 *  Created on: Jan 31, 2016
 *      Author: kjell
 */
#include <iostream>
#include <pthread.h>
#include "Thread.hpp"
#include "global.hpp"

Thread::Thread(ThreadCallBack callBack)
{
	int rc;
	rc = pthread_create(&threadid, NULL, callBack, NULL);
	if (rc) {
		std::cout << "ERROR; return code from pthread_create() = " << rc << std::endl;
	}
}

Thread::Thread(ThreadCallBack callBack, void* argument)
{
	int rc;
	rc = pthread_create(&threadid, NULL, callBack, argument);
	if (rc) {
		std::cout << "ERROR; return code from pthread_create() = " << rc << std::endl;
	}
}

void Thread::Exit(void* arg)
{
	pthread_exit(arg);
}

void Thread::Init()
{
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_attr_destroy(&attr);
}

Thread::~Thread()
{
	void *status;
	int rc;
	rc = pthread_join(threadid, &status);
	if (rc) {
		std::cout << "ERROR; return code from pthread_join() = " << rc << std::endl;
	}
}
