#include <iostream>
#include <sstream>
#include <typeinfo>
#include "Object.hpp"
#include "global.hpp"

std::string Object::ToString()
{
	std::stringstream ss;
	ss << "(" << typeid(Object).name() << ": " << (void*)this<<")";
	return ss.str();
}

Object::~Object()
{
}
