#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <typeinfo>
#include <iostream>
#include <sstream>

#include <unistd.h>
#include <errno.h>

#include "global.hpp"

#include "SerialPort.hpp"

int SerialPort::Open(std::string device)
{
	_Device = device;
	return Open();
}
int SerialPort::Open()
{
	struct termios newtio;

	if (IsOpen())
		Close();

	_fd = open(_Device.c_str(), O_RDWR | O_NOCTTY);
	if (_fd < 0) {
		perror(_Device.c_str());
		return 1;
	}

	tcgetattr(_fd, &_oldtio); /* save current port settings */

	bzero(&newtio, sizeof(newtio));
	newtio.c_cflag = _Baud | CS8 | CLOCAL | CREAD;
	newtio.c_iflag = IGNPAR;
	newtio.c_oflag = 0;

	/* set input mode (non-canonical, no echo,...) */
	newtio.c_lflag = 0;

	newtio.c_cc[VTIME] = 50; /* inter-character timer unused */
	newtio.c_cc[VMIN] = 1; /* blocking read until 1 chars received */

	tcflush(_fd, TCIFLUSH);
	tcsetattr(_fd, TCSANOW, &newtio);
	return 0;
}

tcflag_t SerialPort::GetBaud() const
{
	return _Baud;
}

/** \brief Doesn't set device but sets B9600 as default baud rate. */
SerialPort::SerialPort()
{
	_Baud = B9600;
	_Device = "/dev/arduino";
}

SerialPort::SerialPort(tcflag_t baud)
{
	_Baud = baud;
}

SerialPort::~SerialPort()
{
	Close();
}

int SerialPort::Close()
{
	if (IsOpen()) {
		tcsetattr(_fd, TCSANOW, &_oldtio);
		return DataSource::Close();
	}
	return 0;
}

SerialPort::SerialPort(std::string device)
{
	_Device = device;
	_Baud = B9600;
}

std::string SerialPort::GetDevice() const
{
	return _Device;
}

void SerialPort::SetDevice(std::string Device)
{
	_Device = Device;
}

void SerialPort::SetBaud(tcflag_t Baud)
{
	struct termios newtio;
	_Baud = Baud;

	tcgetattr(_fd, &newtio); /* read current port settings */
	newtio.c_cflag = _Baud | CS8 | CLOCAL | CREAD;
	tcflush(_fd, TCIFLUSH);
	tcsetattr(_fd, TCSANOW, &newtio);
}

const int _baud[] = { 0, 50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400 };
const int _fast_baud[] = { 0, 57600, 115200, 230400, 460800, 500000, 576000, 921600, 1000000, 1152000, 1500000, 2000000, 2500000, 3000000, 3500000, 4000000 };

std::string SerialPort::ToString()
{
	std::stringstream ss;
	int baud = -1;
	if (_Baud < 16)
		baud = _baud[_Baud];
	if (_Baud >= 10001 && _Baud < 10017)
		baud = _fast_baud[_Baud - 10001];

	ss << typeid(SerialPort).name() << ": _Device = \"" << _Device << "\", _Baud = " << baud << ". " << DataSource::ToString();
	return "";//ss.str();
}
